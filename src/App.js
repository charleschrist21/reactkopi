import React, {Component} from 'react';
import Coffee from './component/Coffee';
import Glass from './component/Glass';
import Saucer from './component/Saucer';

export default class App extends Component{
  render(){
    console.log('render');
    return(
      <div>
        <Coffee
        waterVolume={1}
        bean="robusta"
        type="expresso"
        />
        <Glass/>
        <Saucer/>
      </div>
    )
      
    
  }
}